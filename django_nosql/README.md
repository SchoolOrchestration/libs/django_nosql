# django_read_only

> An app that keeps a downstream NoSQL/document database synced with django's

## Installation

```
pip install django_read_only
```

## Usage

### Add to installed apps:

**settings.py**

```python

INSTALLED_APPS = [
    ...
    'django_read_only',
    ...
]
```

configure your readonly backend:

```python
READONLY_BACKENDS = [
    'firebase',
    # feel free to write a backend for any of these :)
    'mongo',
    'couchdb'
    '...'
]
## Backend specific configs:

## Firestore
FIRESTORE_CREDENTIALS_FILE = '/code/appointmentguru-e209c-9f33cd3de0d7.json'
```

### Mark up your models:

```python

class SomeModel(models.Model):

    readonly_sync = True
    serializer = 'path.to.serializer.Serializer'
    collection = 'firebase_collection_name'

    ...

```

# Backends

## FireStore

